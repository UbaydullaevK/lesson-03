package main

import (
  "fmt"
  "math"
)
func main(){
  var loan, interest, period float64
  
  fmt.Println("Enter total amount of loan: ")
  fmt.Scan(&loan)
  var total_loan2 float64 = loan
  fmt.Println("Enter interest rate: ")
  fmt.Scan(&interest)
  fmt.Println("Enter payment period: ")
  fmt.Scan(&period)
  var interestT float64 = float64(interest / 100) / float64(12)
  interest = float64(interest / 100)
  var interest_amount float64 = interest * loan / 12
  fmt.Printf("Interest Amount: %0.2f\n", interest_amount)
  var partMonth = float64(math.Pow((float64(1 + interestT)), period))
  var amountMonth = (loan * interestT * partMonth) / (partMonth - 1)
  var principal = amountMonth - interest_amount
  var balance = loan - principal
  fmt.Printf("Monthly Amount: %0.2f\n", amountMonth)
  fmt.Printf("Balance owed: %0.2f\n", balance)
  fmt.Println("|#\t|Payment\t|Principal\t|Interest\t|Balance Owed")
  var interest_sum float64= 0
  interest_sum += float64(interest_amount)
  var n float64
  for n = 1; n <= period; n++ {
    fmt.Printf("|%v\t|%0.2f\t|%0.2f\t|%0.2f\t|%0.2f\n", n, amountMonth, principal, interest_amount, balance)
    loan = balance
    interest_amount = interest * loan / 12
    principal = amountMonth - interest_amount
    balance = loan - principal
    interest_sum += float64(interest_amount)
  }
  fmt.Printf("Total: %0.2f\n", total_loan2 + interest_sum)
  
}
